
function nombreAleatoire (longeur)
{
    return Math.floor(Math.random() * longeur); 
}

function creationTableau ()
{
    let resultat = "<table>";
    tableau.forEach(ligne => {
        let indexLigne= tableau.indexOf(ligne);
        resultat+="<tr>"
        ligne.forEach(col=>{
           
            let indexCol=ligne.indexOf(col);
            resultat+=`<td> <div>${col}</div>` 
            resultat+=`<div>`
            resultat+=`<button onclick="modifier(${indexLigne},${indexCol})" class='btn  btn-secondary'>Modifier</button> <br>`
            resultat+=`<button onclick="supprimer(${indexLigne},${indexCol})" class='btn  btn-secondary'>X</button> `
            resultat+=`</td>`
        });
        resultat+=`<td><button onclick="ajouter(${indexLigne})" class='btn  btn-secondary'>Ajouter</button></td>`
        resultat+="</tr>"
    });
    resultat+="</table>"
    document.querySelector("#tableau").innerHTML=resultat;
}
function creationDiscours ()
{
    let resultat=""
    for(let numLigne=0;numLigne<4;numLigne++)
    {   
        let laLigne=tableau[numLigne];
        let nombreAlea=nombreAleatoire(laLigne.length);
        let col=laLigne[nombreAlea];
        while(col=="")
        {
        nombreAlea=nombreAleatoire(laLigne.length);
        col=laLigne[nombreAlea];
        }
        
        resultat+=col;
    }
    return resultat;
}
function afficheDiscours ()
{
    document.querySelector("#discours").innerHTML=creationDiscours();
}
// fonctions pour des opérations de type CRUDE
function supprimer(indexLigne,indexCol)
{
    tableau[indexLigne][indexCol]="";
    creationTableau();
}
function modifier (indexLigne,indexCol)
{
    tableau[indexLigne][indexCol]=prompt("Saisir un nouveau texte");
    creationTableau();
}
function ajouter(indexLigne)
{
    let nouvelleValeur=prompt("Saisir une un texte")
    tableau[indexLigne].push(nouvelleValeur);
    creationTableau();
}

//Fin des fonctions
creationTableau()
afficheDiscours()